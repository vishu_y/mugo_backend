require 'rails_helper'

RSpec.describe Api::V1::SessionsController, :type => :controller do
	 describe "Login API" do
	  it "Send auth token after successful login" do
	    user = User.create!(:username => "jdoe", :password => "secret")
	    post "/api/v1/login"
	    assert_select "form.login" do
	      assert_select "input[name=?]", "username"
	      assert_select "input[name=?]", "password"
	      assert_select "input[type=?]", "submit"
	    end

	    post "/api/v1/login", :username => "jdoe", :password => "secret"
	    assert_select ".header .username", :text => "jdoe"
	  end
	end
end
