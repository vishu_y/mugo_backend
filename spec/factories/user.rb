FactoryGirl.define do 
	factory :user do
		name Faker::Name.name
		surname Faker::Name.last_name
	end 
end 