class ApplicationController < ActionController::API
  include ActionController::HttpAuthentication::Token::ControllerMethods
  include ActionController::MimeResponds
  before_filter :cors_preflight_check
  after_filter :cors_set_access_control_headers


  private
    # def restrict_access
    #   restrict_access_by_header || render_unauthorized
    # end

    # def restrict_access_by_header
    #   authenticate_with_http_token do |token, options|
    #     @user = User.find_by_token(token)
    #   end
    # end

    # def render_unauthorized
    # 	self.headers['WWW-Authenticate'] = 'Token realm="Episodes"'
    # 	respond_to do |format|
    # 		format.json {render json: "Bad Credentials", status: 401}
    # 		format.json {render xml: "Bad Credentials", status: 401}
    # 	end
    # end


    def cors_set_access_control_headers
      headers['Access-Control-Allow-Origin'] = '*'
      headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
      headers['Access-Control-Max-Age'] = "1728000"
    end

    # If this is a preflight OPTIONS request, then short-circuit the
    # request, return only the necessary headers and return an empty
    # text/plain.

    def cors_preflight_check
      headers['Access-Control-Allow-Origin'] = '*'
      headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
      headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version'
      headers['Access-Control-Max-Age'] = '1728000'
    end
end
