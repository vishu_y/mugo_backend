class Api::V1::SessionsController < Devise::RegistrationsController
  # prepend_before_filter :require_no_authentication, :only => [:create ]
  # before_filter :ensure_params_exist

  def create
    # user = {email: "dasd@dfdsk.com", password: "dsfsdsdsdf"}
    # params[:user] = user
    # hash = Hash[params.map{ |k, v| [k.to_sym, v] }]

    unless params[:user].blank?
      build_resource
      resource = Api::V1::User.find_by_email(params[:user][:email])
      return invalid_login_attempt unless resource
   
      if resource.valid_password?(params[:user][:email])
        render :json=> {:success=>true, :auth_token=>resource.token}
        return
      end
      invalid_login_attempt
    else
      render :json=> {:success=>false, :message=>"Error with your login or password"}, :status=>401
    end
  end
 
  protected
  def ensure_params_exist
    return unless params[:user][:email].blank?
    render :json=>{:success=>false, :message=>"missing user_login parameter"}, :status=>422
  end
 
  def invalid_login_attempt
    warden.custom_failure!
    render :json=> {:success=>false, :message=>"Error with your login or password"}, :status=>401
  end

  private 
  def hs(key)
      params[key.class == "Symbol" ? key.to_s : key]
  end
end