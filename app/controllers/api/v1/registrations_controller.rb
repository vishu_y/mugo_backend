class Api::V1::RegistrationsController < Devise::RegistrationsController
  # skip_before_filter :verify_authenticity_token

  def create
    # company = {name: "vishu", trademark: "dfd",city: "ddf",website: "dfdf", invoice_address: "dfdf", tax_office: "dfsd", tax_no: "dsfdf", name_coc: "dfdf", reg_no_coc: "dfdf", activity: "dfdf"}
    # user = {name: "vishu", email: "fbbbbvdfdfga@dfdsk.com",surname: "ddf",role: "dfdf", mobile_number: "dfdf", work_number: "dfsd", password: "dsfsdsdsdf", password_confirmation: "dsfsdsdsdf", company_attributes: company}
      user = User.new(user_params)
      if user.save
        render :json=> {:message =>"Register successfully", token: user.token}, :status=>200
        return
      else
        warden.custom_failure!
        render :json=> user.errors.messages, :status=>422
      end
  end

  protected
  def user_params
    #params.require(:user).permit(:name, :surname, :role, :mobile_number, :work_number, company_attributes: [ :name, :trademark,:city,:website,:invoice_address,:tax_office, :tax_no, :name_coc, :reg_no_coc, :activity ])
    params.require(:user).permit!
  end

  def invalid_login_attempt
    render :json=> {:success=>false, :message=>"Error with your login or password"}, :status=>401
  end
end 