class Company < ActiveRecord::Base
	belongs_to :user
	validates_presence_of :name, :trademark,:city,:website,:invoice_address,:tax_office, :tax_no, :name_coc, :reg_no_coc, :activity
end
