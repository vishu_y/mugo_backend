class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :company

  before_save :set_auth_token

  validates_presence_of :name, :surname, :role, :mobile_number, :work_number
  validates_presence_of :password_confirmation, :message => "should match confirmation"

  accepts_nested_attributes_for :company

protected
	
  def set_auth_token
  	return if token.present?
  	self.token = generate_token
  end

  def generate_token
  	loop do
    	token = SecureRandom.hex
    	break token unless self.class.exists?(token: token)
    end
  end
end
