class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :trademark
      t.string :city
      t.string :website
      t.string :invoice_address
      t.string :tax_office
      t.string :tax_no
      t.string :name_coc
      t.string :reg_no_coc
      t.string :activity
      t.integer :user_id
      t.timestamps
    end
  end
end
